# 04.01.Series Temporales

* Series Temporales - Lunes y Jueves de 18 a 22 hs.
* https://exactas-uba.zoom.us/j/84487571021
* ID de reunión: 844 8757 1021
* Código de acceso: st22-infa

## Profesores

* Marcelo Risk
  * Biomedicina
  * CV: 
    * https://www.hospitalitaliano.org.ar/personas/vista.php?idpersona=11572#&despliega=S
    * https://www.conicet.gov.ar/new_scp/detalle.php?id=27019&info_general=yes&inst=yes
  * Mail: marcelo.risk@hospitalitaliano.org.ar
* Federico Albanese
  * Anduvo por Facebook-Meta
  * CV: 
    * https://www.conicet.gov.ar/new_scp/detalle.php?id=54522&keywords=&datos_academicos=yes
  * Contacto: Federico Albanese, ffalbanese@gmail.com


## Links

* Classroom ST: http://157.92.26.246/campus/course/view.php?id=12
* Canal de youtube, con las clases y los TPs finales de los alumnos: https://www.youtube.com/channel/UCIvJy9d0h4uNFW8XiyHzBgA
* GRupos de TP -> https://docs.google.com/spreadsheets/d/1Oi2CrpQVDzvKjHnnKk0Rc7uh7CzExXVnzsFstZVcRbk/edit#gid=0
* Youtube de adrian: https://www.youtube.com/playlist?list=PLcUKhWwmWVPHP73-LeDOQGGXy_fq99Afp

## Modalidad

Metodologia, active learning

* 1 hora de contenido teorico
* TP, y practica en los días
* TP 
  * Max entre 4, puede ser solo
  * final de presentacion oral, y video (subible a youtube)
  * Lunes 26 y Jueves 29 clase presencial, presentacion de temas
    * Mostrar rapidamente
  * Videos de más de 15, se podría, como para explayarse
  * 1/9 avances
  * Tema propuesto por nosotros
  * Sin TP, solo video.

TODO:
* Armar grupos
* Ver temas


## UBA page - Data Mining de Series de Tiempo

**Objetivos**: introducir al maestrando en los principales métodos de análisis de series temporales, tanto desde la teoría cómo con ejemplos prácticos de aplicación.

### Programa

**Unidad 1** Introducción, herramientas y preprocesamiento de series temporales

Definición y representación de series temporales. Medición y adquisición de datos. Representación en los dominios del tiempo y la frecuencia. Convolución, correlación y autocorrelación de series temporales. Métodos de preprocesamiento: representación, indización, segmentación, visualización y medidas de similaridad. Espectrograma. Wavelets. Principal Component Analysis (PCA).

**Unidad 2** Minería de series temporales

Minería de series temporales: descubrimiento de patrones y clustering, clasificación, reglas de descubrimiento, sumarización, predicciones.

**Unidad 3** Aplicaciones

Aplicaciones biomédicas: análisis de la variabilidad de la frecuencia cardiaca a través del electrocardiograma (ECG), análisis del electroencefalograma (EEG). Aplicaciones en negocios y economía: análisis de precios de bienes y servicios, de bonos y acciones de la bolsa.